<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Provides the Admin area functionality / urls for the Downloader module
 *
 * @author 		Phillip Davies
 * @website		http://digitalwrench.co.uk
 * @package 	PyroCMS
 * @subpackage 	Downloader
 */
class Admin extends Admin_Controller
{
    //sets the current section
	protected $section = 'links';


    /**
     * Configures the Admin area validation, required libraries and models
     *
     */
	public function __construct()
	{
		parent::__construct();

		// Load all the required classes
		$this->load->model('downloader_m');
        $this->load->model('downloaderSettings_m');
		$this->load->library('form_validation');
		$this->lang->load('downloader');
        $this->load->library('files/files');

		// Set the validation rules for the create item form
		$this->item_validation_rules = array(
			array(
				'field' => 'name',
				'label' => 'Name',
				'rules' => 'trim|max_length[100]|required'
			),
			array(
				'field' => 'slug',
				'label' => 'Slug',
				'rules' => 'trim|max_length[100]|required'
			),
            array(
                'field' => 'file',
                'label' => 'File',
                'rules' => 'trim|max_length[255]|required|callback_file_check'
            )

		);

        //sets the validation rules for the settings form
        $this->setting_validation_rules = array(
            array(
                'field' => 'base',
                'label' => 'Base URI',
                'rules' => 'trim|max_length[100]|required'
            )
        );

		// We'll set the partials and metadata here since they're used everywhere
		$this->template->append_js('module::admin.js')
						->append_css('module::admin.css');
	}

	/**
	 * List all created download links
	 */
	public function index()
	{
		// here we use MY_Model's get_all() method to fetch everything
		$items = $this->downloader_m->get_all();

        //calls the base url to use in the templates
        $base = $this->downloaderSettings_m->getBase();

		// Build the view with downloader/views/admin/items.php
		$this->template
			->title($this->module_details['name'])
			->set(array('items' => $items, 'base' => $base[0]->base))
			->build('admin/items');
	}

    /**
     * Configures and processes the create download link form
     */
	public function create()
	{

        $name = '';
        $slug = '';
        $base = $this->downloaderSettings_m->getBase();

		// Set the validation rules from the array above
		$this->form_validation->set_rules($this->item_validation_rules);

		// check if the form validation passed
        if($this->input->post())
        {
            if ($this->form_validation->run())
            {
                // See if the model can create the record
                if ($this->downloader_m->create($this->input->post()))
                {
                    // All good...
                    $this->session->set_flashdata('success', lang('downloader:success'));
                    redirect('admin/downloader');
                }
                // Something went wrong. Show them an error
                else
                {
                    $this->session->set_flashdata('error', lang('downloader:error'));
                    redirect('admin/downloader/create');
                }
            }
            else
            {
                $name = $this->input->post('name');
                $slug = $this->input->post('slug');
            }
        }
		
		$downloader = new stdClass;
		foreach ($this->item_validation_rules as $rule)
		{
			$downloader->{$rule['field']} = $this->input->post($rule['field']);
		}

        //get all the files from the uploads folder that matches the base url
        $files = Files::get_files($location = 'local', $container = $base[0]->base);

		// Build the view using downloader/views/admin/form.php
		$this->template
			->title($this->module_details['name'], lang('downloader.new_item'))
			->set(array('downloader' => $downloader, 'name' => $name, 'slug' => $slug, 'files' => $files, 'base' => $base[0]->base))
			->build('admin/form');
	}


    /**
     * Custom validation rule used in the create download link form validation
     *
     * @param String - the input value
     *
     * @return bool
     */
    public function file_check($str)
    {
        if ($str == 'No File Selected')
        {
            $this->form_validation->set_message('file_check', 'The Linked File field is required.');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }


    /**
     * Allows a download link to be deleted and updates the routes to remove dead links.
     *
     * @param int $id
     */
    public function delete($id = 0)
	{
		// make sure the button was clicked and that there is an array of ids
		if (isset($_POST['btnAction']) AND is_array($_POST['action_to']))
		{
			// pass the ids and let MY_Model delete the items
			$this->downloader_m->delete_many($this->input->post('action_to'));
		}
		elseif (is_numeric($id))
		{
			// they just clicked the link so we'll delete that one
			$this->downloader_m->delete($id);
		}

        //redirect to the module homepage in the admin area
		redirect('admin/downloader');
	}


    /**
     * Configures and processes the settings form
     */
    public function settings()
    {

        $base = $this->downloaderSettings_m->getBase();

        // Set the validation rules from the array above
        $this->form_validation->set_rules($this->setting_validation_rules);

        // check if the form validation passed
        if($this->input->post())
        {
            if ($this->form_validation->run())
            {
                // See if the model can create the record
                if ($this->downloaderSettings_m->updateSettings($this->input->post()))
                {
                    // All good...
                    $this->session->set_flashdata('success', lang('downloader:success'));
                    redirect('admin/downloader');
                }
                // Something went wrong. Show them an error
                else
                {
                    $this->session->set_flashdata('error', lang('downloader:error'));
                    redirect('admin/downloader/settings');
                }
            }
            else
            {
                $base = $this->input->post('base');
            }
        }

        $downloader = new stdClass;
        foreach ($this->item_validation_rules as $rule)
        {
            $downloader->{$rule['field']} = $this->input->post($rule['field']);
        }

        // Build the view using downloader/views/admin/form.php
        $this->template
            ->title($this->module_details['name'], lang('downloader:new_item'))
            ->set(array('downloader' => $downloader, 'base' => $base))
            ->build('admin/settingsForm');
    }
}
