<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a sample module for PyroCMS
 *
 * @author 		Jerel Unruh - PyroCMS Dev Team
 * @website		http://unruhdesigns.com
 * @package 	PyroCMS
 * @subpackage 	Sample Module
 */
class Downloader extends Public_Controller
{
    public function __construct()
    {
        parent::__construct();

        // Load the required classes
        $this->load->model('downloader_m');
        $this->load->model('downloaderSettings_m');
        $this->lang->load('downloader');

    }

    /**
     *
     * Remaps the show method call to the index method, keeps it clean yo!
     *
     * @param $method
     * @param array $params
     * @return mixed
     *
     */
    public function _remap($method, $params = array())
    {

        if ($method == 'show')
        {
            return call_user_func_array(array($this, 'index'), $params);
        }

        show_404();
    }

    /**
     * @param null $file String to match the slug in the database
     */
    public function index($file = null)
    {
        $file = $this->downloader_m->getFile($file);

        //if there was no file then fire the 404
        if(sizeof($file) < 1)
            show_404();

        redirect($file[0]->file);

    }

}