<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Downloader extends Module {

	public $version = '1.0';

	public function info()
	{
		return array(
			'name' => array(
				'en' => 'Downloader'
			),
			'description' => array(
				'en' => 'This module allows for file uploads to be linked to a public url for download.'
			),
			'frontend' => FALSE,
			'backend' => TRUE,
            'roles' => array(
                'view_restricted',
                'view_managerial',
                'view_staff'
            ),
			'menu' => 'content', // You can also place modules in their top level menu. For example try: 'menu' => 'downloader',
			'sections' => array(
				'links' => array(
					'name' 	=> 'downloader:links', // These are translated from your language file
					'uri' 	=> 'admin/downloader',
						'shortcuts' => array(
							'create' => array(
								'name' 	=> 'downloader:create',
								'uri' 	=> 'admin/downloader/create',
								'class' => 'add'
								)
							)
						),
                'settings' => array(
                        'name' 	=> 'downloader:settings', // These are translated from your language file
                        'uri' 	=> 'admin/downloader/settings'
                        )
                    )
		);
	}

	public function install()
	{

		$this->dbforge->drop_table('downloader');
        $this->dbforge->drop_table('downloaderSettings');
		$this->db->delete('settings', array('module' => 'downloader'));


        $tables = array(
            'downloader' => array(
                'id' => array('type' => 'INT', 'constraint' => 11, 'auto_increment' => true, 'primary' => true),
                'name' => array('type' => 'VARCHAR', 'constraint' => 100),
                'slug' => array('type' => 'VARCHAR', 'constraint' => 100),
                'file' => array('type' => 'VARCHAR', 'constraint' => 255),
            ),
            'downloaderSettings' => array(
                            'id' => array(
                                'type' => 'INT',
                                'constraint' => '11',
                                'auto_increment' => TRUE,
                                'primary' => true
                            ),
                            'base' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255'
                            )
                )
        );


        $baseSettings = array(
          'base' => 'docs'
        );



        if(!$this->install_tables($tables) || !$this->db->insert('downloaderSettings', $baseSettings))
        {
            return false;
        }


        //set the slug column to be unique
        if(!$this->db->query('ALTER TABLE default_downloader ADD UNIQUE (slug)'))
        {
            return FALSE;
        }

        return true;
	}

	public function uninstall()
	{
		$this->dbforge->drop_table('downloader');
        $this->dbforge->drop_table('downloaderSettings');

		$this->db->delete('settings', array('module' => 'downloader'));
		{
			return TRUE;
		}
	}


	public function upgrade($old_version)
	{
		// Your Upgrade Logic
		return TRUE;
	}

	public function help()
	{
		// Return a string containing help info
		// You could include a file and return it here.
		return "No documentation has been added for this module.<br />Contact the module developer for assistance. support@digitalwrench.co.uk";
	}
}
/* End of file details.php */
