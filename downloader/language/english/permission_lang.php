<?php defined('BASEPATH') OR exit('No direct script access allowed');

//permissions
$lang['downloader:role_view_restricted']			=	'View Restricted Files';
$lang['downloader:role_view_managerial']			=	'View Managerial Files';
$lang['downloader:role_view_staff']		=	'View Staff files';
