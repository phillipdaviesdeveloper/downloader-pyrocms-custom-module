<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This Class provides access to the download links database table
 *
 * @author 		Phillip Davies
 * @website		http://digitalwrench.co.uk
 * @package 	PyroCMS
 * @subpackage 	Downloader
 */
class Downloader_m extends MY_Model {

	public function __construct()
	{		
		parent::__construct();

        //get the download helper
        $this->load->helper('download');

        /**
         * Sets the Class table for queries
         */
		$this->_table = 'downloader';
	}


    /**
     * Creates a new download link record
     *
     * @param Array
     *
     * @return bool
     */
	public function create(array $input)
	{
        //hydrate the insert item
		$to_insert = array(
			'name' => $input['name'],
			'slug' => $this->_check_slug($input['slug']),
            'file' => $this->_clear_path($input['file'])
		);

        //insert the new item into the database
        if(!$this->db->insert('downloader', $to_insert))
        {
            return false;
        }

		return true;
	}

    /**
     * Checks that the provided URI slug is in a valid format
     *
     * @param String
     *
     * @return String
     */
	public function _check_slug($slug)
	{
		$slug = strtolower($slug);
		$slug = preg_replace('/\s+/', '-', $slug);

		return $slug;
	}

    /**
     * Removes the {{url:site}} tag from the file path (why is this even there??)
     *
     * @param String
     *
     * @return String
     */
    public function _clear_path($path)
    {
        return preg_replace("/{{ url:site }}/", "", $path);
    }


    /**
     *
     * Grabs the file associated with the uri string
     *
     * @param $file
     * @return mixed
     */
    public function getFile($file)
    {
        $this->db->select('file, name');
        $this->db->where('slug', $file);
        $this->db->limit(1);

        $query = $this->db->get($this->_table);

        return $query->result();

    }

}
