<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This Class provides access to the Base URL setting
 *
 * @author 		Phillip Davies
 * @website		http://digitalwrench.co.uk
 * @package 	PyroCMS
 * @subpackage 	Downloader
 */
class DownloaderSettings_m extends MY_Model {

	public function __construct()
	{		
		parent::__construct();
		
		/**
         * Sets the Class table for queries
		 */
		$this->_table = 'downloaderSettings';
	}

    /**
     * Updates the Base URL setting
     *
     * @param Array
     *
     * @return bool
     */
	public function updateSettings($input)
	{
        $this->load->model('downloader_m');

		$to_insert = array(
			'base' => $input['base'],
		);

        if(!$this->db->update($this->_table, $to_insert))
            return false;

		return true;
	}

    /**
     * returns the configured Base URI
     *
     * @return bool || Object
     */
	public function getBase()
	{
        $this->db->select('base');
        $this->db->limit(1);

        return $this->db->get($this->_table)->result();
	}
}
