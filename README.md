# PyroCMS Downloader Module

## Legal

This module is built using the sample module originally written by [Jerel Unruh](http://twitter.com/jerelunruh), one of the PyroCMS core developers but is now part of PyroCMS Documentation.


## Description

This basic module was written to allow download urls to be created in the control panel.

Current limitations:

    1) Not optimised for SEO, dead links will require pruning, active links may require adding to the sitemap.xml

    2) Does not check that links are valid for the system, therefore urls may collide if set in other parts of pyro (e.g. the page /download
    is created when you have setup a download link called downloads. Links added in the download module are however checked to be unique by a database constraint.

    3) Currently it only reads from the file container that matches the base URI configured in the module settings (downloader/settings)

    4) The system/cms/config/routes.php file must be amended if you do not want to use downloader/*file slug* as your download link


## Usage

To use this module simply clone it using Git or download the zip.  Once you have the folder you will need to rename it to "downloader". This is very important as
PyroCMS uses the folder name to determine the class to call when installing or when loading a controller. This may mean that you will need to unzip the
downloaded version and then rezip it before uploading to your PyroCMS install.

Simply past the module in your addons folder under 'modules' and then navigate to the addons section in your control panel to enable the module.

You will need to modify your system/cms/config/routes.php with the following line if you want to use a url other than 'downloader' in your file downloads:

    //the url you want e.g. docs                  //point to the module frontend controller
    $route['docs/(:any)']                       = 'downloader/show/$1';
